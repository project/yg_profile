<?php

/**
 * @file
 * YG Profile theme implementations.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_menu().
 */
function yg_profile_preprocess_menu(&$variables, $hook) {
  // We're doing that for main menu.
  if ($hook == 'menu__main') {
    // Get the current path.
    $current_path = \Drupal::request()->getRequestUri();
    $items = $variables['items'];
    foreach ($items as $key => $item) {
      // If path is current_path, set active to li.
      if ($item['url']->toString() == $current_path) {
        // Add active link.
        $variables['items'][$key]['attributes']['class'] = 'active';
      }
    }
  }
}

/**
 * Implements hook_preprocess_html().
 */
function yg_profile_preprocess_html(&$variables) {
  $variables['logo_url'] = theme_get_setting('logo.url');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function yg_profile_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  $form['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];
  $form['banner'] = [
    '#type' => 'details',
    '#title' => t('Background Image'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['banner']['bg-image'] = [
    '#type' => 'managed_file',
    '#title'    => t('Background Image'),
    '#default_value' => theme_get_setting('bg-image'),
    '#upload_location' => 'public://',
    '#description' => t('Choose your background image for 404,403 pages'),
  ];

  $form['#submit'][] = 'yg_profile_form_submit';

}

/**
 * Implements hook_form_submit().
 */
function yg_profile_form_submit(&$form, $form_state) {
  $fid = $form_state->getValue('bg-image');
  if (array_key_exists(0, $fid)) {
    $file = File::load($fid[0]);
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'yg_profile', 'themes', 1);
    }
  }

}

/**
 * Implements hook_preprocess_page().
 */
function yg_profile_preprocess_page(&$variables) {
  $variables['copyright'] = theme_get_setting('copyright')['value'];
  $variables['basepath'] = $GLOBALS['base_url'];

  $bg_image_url = [];
  $path = [];
  $fid = theme_get_setting('bg-image', 'yg_profile')[0];
  for ($i = 1; $i <= $fid; $i++) {
    if (!empty($fid)) {
      $file = File::load($fid);
      $path = Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    }
  }
  $variables['bg_image_url'] = $path;

}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function yg_profile_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  // Get Request Object.
  $request = \Drupal::request();

  // If there is HTTP Exception..
  if ($exception = $request->attributes->get('exception')) {
    // Get the status code.
    $status_code = $exception->getStatusCode();
    if (in_array($status_code, [401, 403, 404])) {
      $suggestions[] = 'page__' . $status_code;
    }
  }
}

/**
 * Implements hook_theme().
 */
function yg_profile_theme(&$existing, $type, $theme, $path) {
  $hooks = [];
  $hooks['user_login_form'] = [
    'render element' => 'form',
    'template' => 'user-login-form',
  ];
  return $hooks;
}

/**
 * Implements hook_library_info_alter().
 */
function yg_profile_library_info_alter(&$libraries, $ext) {
  unset($libraries['form']);
}
